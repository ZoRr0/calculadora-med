#ifndef MDRD_H
#define MDRD_H

#include <QWidget>

namespace Ui {
class mdrd;
}

class mdrd : public QWidget
{
    Q_OBJECT

public:
    explicit mdrd(QWidget *parent = 0);
    ~mdrd();

private slots:
    void on_pushButton_clicked();

private:
    Ui::mdrd *ui;
};

#endif // MDRD_H

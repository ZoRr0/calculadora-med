#ifndef IMCAM_H
#define IMCAM_H

#include <QWidget>

namespace Ui {
class imcam;
}

class imcam : public QWidget
{
    Q_OBJECT

public:
    explicit imcam(QWidget *parent = nullptr);
    ~imcam();

private slots:
    void on_boton_clicked();

private:
    Ui::imcam *ui;
};

#endif // IMCAM_H

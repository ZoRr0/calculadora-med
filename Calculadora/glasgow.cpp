#include "glasgow.h"
#include "ui_glasgow.h"

glasgow::glasgow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::glasgow)
{
    ui->setupUi(this);
}

glasgow::~glasgow()
{
    delete ui;
}

void glasgow::on_pushButton_clicked()
{
    //Variables
    int o=0,v=0,m=0,g=0;
    //Dar puntaje a partir de la selección
    if (ui->OE->isChecked()){
        o = 4;
        }
        else {
            if(ui->OS->isChecked()){
                o = 3;
            }
            else {
                if(ui->OP->isChecked()){
                    o = 2;
                }
                else{
                    if(ui->ON->isChecked()){
                        o=1;
                    }
                    else{
                        o=0;
                    }
                }
            }
    }
    if (ui->VO->isChecked()){
        v=5;
    }
    else{
        if(ui->VC->isChecked()){
            v=4;
        }
        else{
            if(ui->VP->isChecked()){
                v=3;
            }
            else{
                if(ui->VS->isChecked()){
                    v=2;
                }
                else {
                    if(ui->VN->isChecked()){
                        v=1;
                    }
                    else{
                        v=0;
                    }
                }
            }
        }
    }
    if(ui->MO->isChecked()){
        m=6;
    }
    else{
        if(ui->ML->isChecked()){
            m=5;
        }
        else{
            if(ui->MFN->isChecked()){
                m=4;
            }
            else{
                if(ui->MFAN->isChecked()){
                    m=3;
                }
                else{
                    if(ui->ME->isChecked()){
                        m=2;
                    }
                    else{
                        if(ui->MN->isChecked()){
                            m=1;
                        }
                        else {
                            m=0;
                        }
                    }
                }
            }
        }
    }
    //Formula
    g=o+v+m;

    //Clasificación
    if(g>=13 && o>0 && v>0 && m>0){
        ui->cla->setText("Se Considera un Traumatismo \nLeve, según la OMS.");
    }
    else{
        if(g>=9 && g<=12 && o>0 && v>0 && m>0){
            ui->cla->setText("Se Considera un Traumatismo\nModerado, según la OMS.");
        }
        else{
            if(g<=8 && o>0 && v>0 && m>0){
                ui->cla->setText("Se considera un traumatismo Grave,\nque amerita \nintubación endotraqueal.");
            }else {
                if(g==0 && o==0 && v==0 && m==0){
                    ui->cla->setText("Todo es NV, revisa las opciones seleccionadas.");
                }
                ui->cla->setText("Tienes un resultado NV,\nsin posibilidad de clasificar\nadecuadamente.");
            }
        }
    }

    //Resultados // REVISAR TOTAL Y CLASIFACIÓM, DEJAR EL TOTAL COMO ESTA, SOLO LA OPCIPON DE TODAS NO VALORABLES PUEDE SER
    if (o==0){
        QString text = "Ocular: NV";
        ui->ocu->setText(text);
    }
    else{
        QString text = "Ocular: " +QString::number(o);
        ui->ocu->setText(text);
    }if (v==0){
        QString text = "Verbal: NV";
        ui->ver->setText(text);
    }
    else{
        QString text = "Verbal: " + QString::number(v);
        ui->ver->setText(text);
    }if (m==0){
        QString text = "Motor: NV";
        ui->mot->setText(text);
    }
    else{
        QString text = "Motor: " + QString::number(m);
        ui->mot->setText(text);
    }if (g==0){
        QString text = "Todas las respuestas son NV.";
        ui->total->setText(text);
    }else{
        QString text = "Total: " + QString::number(g);
        ui->total->setText(text);
    }
}

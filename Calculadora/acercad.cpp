#include "acercad.h"
#include "ui_acercad.h"

acercaD::acercaD(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::acercaD)
{
    ui->setupUi(this);
}

acercaD::~acercaD()
{
    delete ui;
}

void acercaD::on_grax_clicked()
{
   QString text = "Calculadora-MED\n"
                  "Creado:02/09/18\n"
                  "Modificado: 25/08/2021\n"
                  "Versión 1.0.7.1 (25/08/2021)\n"
                  " Se agrega la opción de elección manual de cantidad de potasio en calculo pediatrico y conversión del mismo en mililitros\n"
                  "Versión 1.0.7 (08/10/2020)\n"
                  " Se corrige calculo de soluciones pediátrica, ya que no permitía calculo con decimales\n"
                  "Versión 1.0.6 (30/06/2020)\n"
                  " Se agrega calculo de soluciones pediátricas por formula de Holliday-Segar\n"
                  "Versión 1.0.5 (24/06/2020)\n"
                  " Se agrega calculo para velocidad de infusión de una solución\n"
                  " Corrección en la interfaz de Dosis Ponderal\n"
                  "Versión 1.0.4.1 (06/06/2020)\n"
                  " Corrección en la presentación de resultados de la escala de Glasgow\n"
                  "Versión 1.0.4 (03/06/2020)\n"
                  " Se agrega calculo para dosis ponderal\n"
                  "Versión 1.0.3.1 (03/06/2020)\n"
                  " Se agrega icono de la aplicación para Android\n"
                  "Versión 1.0.3 (31/5/2020)\n"
                  " Se agrega calculo de IMC con corrección de altura\npor formula de altura talón-rodilla\n"
                  "Versión 1.0.2.1 (12/9/19)\n"
                  " Corrección menores y mejoras en el calculo del IMC\ncon nuevos resultados de peso ideal y diferencia con el actual.\n"
                  "Versión 1.0.2 (24/10/18)\n"
                  " Se agrega el calculo de Superficie Corporal\n"
                  "Versión 1.0.1 (02/10/18)\n"
                  " Corrección de error al seleccionar en la escala de Glasgow\n"
                  "Versión 1.0 (02/09/18)\n"
                  " Lanzamiento de la aplicación\n"
                  "Calculadora-MED es un programa que esta dirigido como apoyo, \nprincipalmente a los médicos y/o personal de salud.\n"
                  "Por ningún motivo o razón deberá tomarse los resultados \naquí mostrados como hechos verdaderos,\n"
                  "estos siempre deberán ser interpretados, analizados y \nusados bajo experiencia profesional.\n"
                  "\n"
                  "Agradecimiento especial al Dr. Enrique Escobar Escobar,\nquien fue mi guía, maestro y amigo durante\n"
                  "todos estos años.\n"
                  "También a mi familia que es mi apoyo y sostén\npara todo lo que hago.\n"
                  "Por supuesto a ti mon cherie (ccy), quien eres la razón\npor la que mejoro cada día, gracias.\n:)"
                  "\n"
                  "Contacto:\n"
                  "Correo Electrónico: pedro.p.rc@riseup.net\n"
                  "Telegram: https://t.me/ZoRrO08\n"
                  "Código Fuente\n"
                  "https://gitlab.com/ZoRr0/calculadora-med/-/tree/master/\n"
                  "Donaciones\n"
                  "BTC: 1Gih8r3efVWGbhrbU7cn5mWHGQTvAsojPT\n"
                  "\n"
                  "Calculadora-MED\n"
                  "Copyright 2018 Pedro Pablo Reyes Cameras <pedro.p.rc@riseup.net>\n"
                  "\n"
                  "This progrAm is free software; you can redistribute it and/or modify\n"
                  "it under the terms of the GNU General Public License as published by\n"
                  "the Free Software Foundation; either version 3 of the License, or\n"
                  "(at your option) any later version.\n"
                  "\n"
                  "This program is distributed in the hope that it will be useful,\n"
                  "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
                  "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
                  "GNU General Public License for more details.\n"
                  "\n"
                  "You should have received a copy of the GNU General Public License\n"
                  "along with this program.  If not,\nsee <http://www.gnu.org/licenses/> or <https://www.gnu.org/licenses/gpl.txt>.\n"
                  "\n"
                  "TRADUCCIÓN NO OFICIAL\n"
                  "Calculadora-MED\n"
                  "Derechos de Autor 2018 Pedro Pablo Reyes Cameras <pedro.p.rc@riseup.net>\n"
                  "\n"
                  "Este programa es software libre; puedes redistribuirlo y/o modificarlo\n"
                  "bajo los términos de la Licencia Pública General GNU publicada por la Fundación para el Software Libre;\n"
                  "ya sea la versión 3 de la Licencia, o (a su elección)cualquier versión posterior.\n"
                  "Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA; ni siquiera\n"
                  "la garantía implícita de COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR.\n"
                  "Vea la Licencia Pública General GNU para más detalles.\n"
                  "Debería haber recibido una copia de la Licencia Pública General de GNU junto con este programa; si no,\n"
                  "visite <http://www.gnu.org/licenses/> o <https://www.gnu.org/licenses/gpl.txt>.";
   ui->acD->setText(text);

}

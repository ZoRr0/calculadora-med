#ifndef HIPONAP_H
#define HIPONAP_H

#include <QWidget>

namespace Ui {
class hiponaP;
}

class hiponaP : public QWidget
{
    Q_OBJECT

public:
    explicit hiponaP(QWidget *parent = 0);
    ~hiponaP();

private slots:
    void on_pushButton_clicked();

private:
    Ui::hiponaP *ui;
};

#endif // HIPONAP_H

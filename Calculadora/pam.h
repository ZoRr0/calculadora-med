#ifndef PAM_H
#define PAM_H

#include <QWidget>

namespace Ui {
class PAM;
}

class PAM : public QWidget
{
    Q_OBJECT

public:
    explicit PAM(QWidget *parent = 0);
    ~PAM();

private slots:
    void on_pushButton_clicked();

private:
    Ui::PAM *ui;
};

#endif // PAM_H

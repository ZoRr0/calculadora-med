#include "calculodosis.h"
#include "ui_calculodosis.h"

calculoDosis::calculoDosis(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::calculoDosis)
{
    ui->setupUi(this);
}

calculoDosis::~calculoDosis()
{
    delete ui;
}

void calculoDosis::on_pushButton_clicked()
{
// Obtener valores desde la interfaz a las variables temporales
QString tkg = ui->kg->text();
QString tml = ui->ml->text();
QString tmg = ui->mg->text();
QString tmin = ui->minima->text();
QString tmax = ui->maxima->text();
// Convertir los temporales en variables a usar
float kg = tkg.toFloat();
int ml = tml.toInt();
int mg = tmg.toInt();
float min = tmin.toFloat();
float max = tmax.toFloat();
// Formulas
float tresultadoMin = kg*min;
float resultadoMin = (tresultadoMin*ml)/mg;
float tresultadoMax = kg*max;
float resultadoMax = (tresultadoMax*ml)/mg;
if(ui->ocho->isChecked()){
   float resultadoFM=resultadoMax/3;
   float resultadoFMG=resultadoFM*20;
   float resultadoFMin=resultadoMin/3;
   float resultadoFMinG=resultadoFMin*20;
   QString result = "Cantidad MAXIMA en ml\nes de: " + QString::number(resultadoFM) + " ml,\nlo que equivale a: "+QString::number(resultadoFMG)+" gotas\n" + "Cantidad MINIMA en ml\nes de: " + QString::number(resultadoFMin) + " ml,\nlo que equivale a: "+ QString::number(resultadoFMinG)+" gotas\n";
   ui->resultados->setText(result);
}else{
    if(ui->dosis->isChecked()){
        float resultadoFM=resultadoMax;
        float resultadoFMG=resultadoFM*20;
        float resultadoFMin=resultadoMin;
        float resultadoFMinG=resultadoFMin*20;
        QString result = "Cantidad MAXIMA en ml\nes de: " + QString::number(resultadoFM) + " ml,\nlo que equivale a: "+QString::number(resultadoFMG)+" gotas\n" + "Cantidad MINIMA en ml\nes de: " + QString::number(resultadoFMin) + " ml,\nlo que equivale a: "+ QString::number(resultadoFMinG)+" gotas\n";
        ui->resultados->setText(result);
        }
    else {
        if(ui->seis->isChecked()){
            float resultadoFM=resultadoMax/4;
            float resultadoFMG=resultadoFM*20;
            float resultadoFMin=resultadoMin/4;
            float resultadoFMinG=resultadoFMin*20;
            QString result = "Cantidad MAXIMA en ml\nes de: " + QString::number(resultadoFM) + " ml,\nlo que equivale a: "+QString::number(resultadoFMG)+" gotas\n" + "Cantidad MINIMA en ml\nes de: " + QString::number(resultadoFMin) + " ml,\nlo que equivale a: "+ QString::number(resultadoFMinG)+" gotas\n";
            ui->resultados->setText(result);
        }
        else{
            if(ui->cuatro->isChecked()){
                float resultadoFM=resultadoMax/6;
                float resultadoFMG=resultadoFM*20;
                float resultadoFMin=resultadoMin/6;
                float resultadoFMinG=resultadoFMin*20;
                QString result = "Cantidad MAXIMA en ml\nes de: " + QString::number(resultadoFM) + " ml,\nlo que equivale a: "+QString::number(resultadoFMG)+" gotas\n" + "Cantidad MINIMA en ml\nes de: " + QString::number(resultadoFMin) + " ml,\nlo que equivale a: "+ QString::number(resultadoFMinG)+" gotas\n";
                ui->resultados->setText(result);
            }
            else{
                float resultadoFM=resultadoMax/2;
                float resultadoFMG=resultadoFM*20;
                float resultadoFMin=resultadoMin/2;
                float resultadoFMinG=resultadoFMin*20;
                QString result = "Cantidad MAXIMA en ml\nes de: " + QString::number(resultadoFM) + " ml,\nlo que equivale a: "+QString::number(resultadoFMG)+" gotas\n" + "Cantidad MINIMA en ml\nes de: " + QString::number(resultadoFMin) + " ml,\nlo que equivale a: "+ QString::number(resultadoFMinG)+" gotas\n";
                ui->resultados->setText(result);
            }
        }
    }
}
}

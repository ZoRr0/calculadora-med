#include "diuresis.h"
#include "ui_diuresis.h"

diuresis::diuresis(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::diuresis)
{
    ui->setupUi(this);
}

diuresis::~diuresis()
{
    delete ui;
}

void diuresis::on_pushButton_clicked()
{
    float DT,T,P, DMH, DKH;
    QString Dt = ui->dt->text();
    DT = Dt.toFloat();
    QString Tt = ui->t->text();
    T = Tt.toFloat();
    QString Pt = ui->p->text();
    P = Pt.toFloat();
    DMH = DT/T;
    DKH = DMH/P;
    ui->dmh->setNum(DMH);
    ui->dkh->setNum(DKH);
}

#include "imc-am.h"
#include "ui_imc-am.h"

imcam::imcam(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::imcam)
{
    ui->setupUi(this);
}

imcam::~imcam()
{
    delete ui;
}

void imcam::on_boton_clicked()
{
    //Obtener valores de la interfaz y colocarlo en una variable temporal
    QString tedad = ui->edad->text();
    QString tatr = ui->atr->text();
    QString tpeso = ui->peso->text();
    QString clas; //Variable a usar para la clasificación de IMC
    // Convertir las variables temporales a las utilizables en el código
    int Edad = tedad.toInt();
    float ATR = tatr.toFloat();
    float Peso = tpeso.toFloat();
    double imc2;
    float alturaC;
    // Formulas
    if(ui->mujer->isChecked()){
        alturaC = (73.7+(1.99*ATR)-(0.23*Edad))/100;
        imc2 = Peso/(alturaC*alturaC);
    }else {
        alturaC =(52.6+(2.17*ATR))/100;
        imc2 = Peso/(alturaC*alturaC);
        }
if(imc2<16){
        clas = "Se considera: Infrapeso" ;
        }
        else{
            if(imc2<=16){
                clas= "Se considera: Delgadez severa";
                }
            else{
                if(imc2<17){
                    clas = "Se considera: Delgadez moderada";
                    }
                else{
                    if(imc2<=18.49){
                    clas = "Se considera: Delgadez no muy pronunciada";
                    }
                    else{
                        if(imc2<=24.99){
                            clas = "Se considera: Normal";
                        }
                        else{
                            if(imc2==25){
                                clas = "Se considera: Sobrepeso";
                            }
                            else{
                                if(imc2<=29.99){
                                    clas = "Se considera: Preobeso";
                                }
                                else{
                                    if(imc2==30){
                                        clas = "Se considera: Obesidad";
                                    }
                                    else{
                                        if(imc2<=34.99){
                                            clas = "Se considera: Obesidad Tipo I";
                                        }
                                        else{
                                            if(imc2<=39.99){
                                                clas = "Se considera: Obesidad Tipo II";
                                            }
                                            else{
                                                clas = "Se considera: Obesidad Tipo III";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

double pIA=(alturaC*alturaC)*24.99;
double pIB=(alturaC*alturaC)*18.5;
if (imc2>=25){
        double D= Peso-pIA;
        QString result = "Tu IMC corregido es de: " + QString::number(imc2) + " Kg/m2\n" + clas + "\nTú peso máximo ideal es de: " + QString::number(int(pIA)) + " Kg\nTú peso mínimo ideal es de: " + QString::number(int(pIB)) + " Kg\nTienes una diferencia con tu peso ideal máximo de:\n" + QString::number(int(D)) + " Kg\n:(\n";
        ui->resultados->setText(result);
        }
        else{
                if(imc2<=18.5){
                        double D=pIB-Peso;
                        QString result = "Tu IMC corregido es de: " + QString::number(imc2) + " Kg/m2\n" + clas + "\nTú peso máximo ideal es de: " + QString::number(int(pIA)) + " Kg\nTú peso mínimo ideal es de: " + QString::number(int(pIB)) + " Kg\nTienes una diferencia con tu peso ideal mínimo de:\n" + QString::number(int(D)) + " Kg\n:(\n";
                        ui->resultados->setText(result);
                        }
                        else{
                             QString result = "Tu IMC corregido es de: " + QString::number(imc2) + " Kg/m2\n" + clas + "\nTú peso máximo ideal es de: " + QString::number(int(pIA)) + " Kg\nTú peso mínimo ideal es de: " + QString::number(int(pIB)) + " Kg\nEstas dentro de tu peso ideal.\nFELICIDADES :)\n";
                             ui->resultados->setText(result);
                        }
                }

}

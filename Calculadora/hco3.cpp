#include "hco3.h"
#include "ui_hco3.h"

hco3::hco3(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::hco3)
{
    ui->setupUi(this);
}

hco3::~hco3()
{
    delete ui;
}

void hco3::on_calcular_clicked()
{
    ///Variables
    QString pes = ui->peso->text();
    QString Hco3D = ui->hco3D->text();
    QString Hco3A = ui->hco3A->text();
    float Pes = pes.toFloat();
    float HCO3A = Hco3A.toFloat();
    float HCO3D = Hco3D.toFloat();
    ///Formulas
    float defict = Pes*(0.4+(2.4/HCO3A));
    int defict2 = defict*(HCO3D-HCO3A);
    ///Resultados
    QString text = "El déficit es de: " + QString::number(defict2) + " mEq.";
    ui->res->setText(text);
}

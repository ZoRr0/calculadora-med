#include "hiponar.h"
#include "ui_hiponar.h"

hipoNaR::hipoNaR(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::hipoNaR)
{
    ui->setupUi(this);
}

hipoNaR::~hipoNaR()
{
    delete ui;
}

void hipoNaR::on_pushButton_clicked()
{
    //Conversión de Variables
    QString PESO = ui->peso->text();
    QString NAS = ui->NaS->text();

    //Conversión y declaración de variables a usar
    int nas = NAS.toInt();
    float peSo = PESO.toFloat();
    int NaSol;
    float ACT;
    if(ui->masculino->isChecked()){
        ACT=0.6;
        }
        else{
        ACT=0.5;
            }
    if(ui->al09->isChecked()){
            NaSol=154;
            }
            else{
                if(ui->al15->isChecked()){
                    NaSol=256;
                    }
                    else{
                        NaSol=514;
                        }
            }
    //Formulas
           float R = (NaSol-nas)/((peSo*ACT)+1);

    //Presentación de Resultados
           if(ui->masculino->isChecked()){
               QString text = "Una Solución con " + QString::number( NaSol ) + " mEq de Sodio en\n un paciente masculino de " + QString::number( peSo ) + " Kg de peso, se\n logra restituir un total de " + QString::number(R) + " mEq\n con la solución elegida.";
               ui->res->setText(text);
               }
               else{
               QString text = "Una Solución con " + QString::number( NaSol ) + " mEq de Sodio en\n un paciente femenino de " + QString::number( peSo ) + " Kg de peso, se\n logra restituir un total de " + QString::number(R) + " mEq\n con la solución elegida.";
               ui->res->setText(text);
                   }
       }


#ifndef CALCULODOSIS_H
#define CALCULODOSIS_H

#include <QWidget>

namespace Ui {
class calculoDosis;
}

class calculoDosis : public QWidget
{
    Q_OBJECT

public:
    explicit calculoDosis(QWidget *parent = nullptr);
    ~calculoDosis();

private slots:
    void on_pushButton_clicked();

private:
    Ui::calculoDosis *ui;
};

#endif // CALCULODOSIS_H

#include "hiponap.h"
#include "ui_hiponap.h"

hiponaP::hiponaP(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::hiponaP)
{
    ui->setupUi(this);
}

hiponaP::~hiponaP()
{
    delete ui;
}

void hiponaP::on_pushButton_clicked()
{
    //Conversión de Variables
    QString CANT = ui->cant->text();

    //Conversión y declaración de variables
    float cant = CANT.toFloat();
    double R;
    double ampu;
    //Formulas
    if(ui->porC->isChecked()){
            R = (cant*154)/0.8981;
            ampu = (R-154)/30;
            QString text = "Debes administrar " + QString::number(int (ampu)) + " ampolletas de \nNaCl al 17.7%, si deseas que tu solución \nal 0.9% se convierta al " + QString::number(cant) + "% y tenga " +  QString::number(int (R)) + " mEq.";
            ui->res->setText(text);
            }
            else{
                R = (cant*0.9)/154;
                ampu = (cant-154)/30;
                QString text = "Debes administrar " + QString::number(int (ampu)) + " ampoyetas de \nNaCl al 17.7%, si deseas que tu solución \nal 0.9% tenga " + QString::number(cant) + " mEq y \nsea una solución al " +  QString::number(R) + "%.";
                ui->res->setText(text);

                }

}

#ifndef ACERCAD_H
#define ACERCAD_H

#include <QWidget>

namespace Ui {
class acercaD;
}

class acercaD : public QWidget
{
    Q_OBJECT

public:
    explicit acercaD(QWidget *parent = 0);
    ~acercaD();

private slots:
    void on_grax_clicked();

private:
    Ui::acercaD *ui;
};

#endif // ACERCAD_H

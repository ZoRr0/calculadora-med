#ifndef IMC_H
#define IMC_H

#include <QWidget>

namespace Ui {
class imc;
}

class imc : public QWidget
{
    Q_OBJECT

public:
    explicit imc(QWidget *parent = 0);
    ~imc();

private slots:
    void on_pushButton_clicked();

private:
    Ui::imc *ui;
};

#endif // IMC_H

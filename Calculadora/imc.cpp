#include "imc.h"
#include "ui_imc.h"

imc::imc(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::imc)
{
    ui->setupUi(this);
}

imc::~imc()
{
    delete ui;
}

void imc::on_pushButton_clicked()
{
    QString clas;
    QString pes = ui->peso->text();
    float Pes = pes.toFloat();
    QString al = ui->altura->text();
    float Al = al.toFloat();
    double imc2 = Pes/(Al*Al);
    ui->res->setNum(imc2);
       if(imc2<16){
               clas = "Se considera: Infrapeso" ;
               }
               else{
                   if(imc2<=16){
                       clas= "Se considera: Delgadez severa";
                       }
                   else{
                       if(imc2<17){
                           clas = "Se considera: Delgadez moderada";
                           }
                       else{
                           if(imc2<=18.49){
                           clas = "Se considera: Delgadez no muy pronunciada";
                           }
                           else{
                               if(imc2<=24.99){
                                   clas = "Se considera: Normal";
                               }
                               else{
                                   if(imc2==25){
                                       clas = "Se considera: Sobrepeso";
                                   }
                                   else{
                                       if(imc2<=29.99){
                                           clas = "Se considera: Preobeso";
                                       }
                                       else{
                                           if(imc2==30){
                                               clas = "Se considera: Obesidad";
                                           }
                                           else{
                                               if(imc2<=34.99){
                                                   clas = "Se considera: Obesidad Tipo I";
                                               }
                                               else{
                                                   if(imc2<=39.99){
                                                       clas = "Se considera: Obesidad Tipo II";
                                                   }
                                                   else{
                                                       clas = "Se considera: Obesidad Tipo III";
                                                   }
                                               }
                                           }
                                       }
                                   }
                               }
                           }
                       }
                   }
               }

       double pIA=(Al*Al)*24.99;
       double pIB=(Al*Al)*18.5;
       if (imc2>=25){
               double D= Pes-pIA;
               QString result = clas + "\nTú peso máximo ideal es de: " + QString::number(int(pIA)) + " Kg\nTú peso mínimo ideal es de: " + QString::number(int(pIB)) + " Kg\nTienes una diferencia con tu peso ideal máximo de:\n" + QString::number(int(D)) + " Kg\n:(\n";
               ui->cla->setText(result);
               }
               else{
                       if(imc2<=18.5){
                               double D=pIB-Pes;
                               QString result = clas + "\nTú peso máximo ideal es de: " + QString::number(int(pIA)) + "\nTú peso mínimo ideal es de: " + QString::number(int(pIB)) + "\nTienes una diferencia con tu peso ideal mínimo de:\n" + QString::number(int(D)) + " Kg\n:(\n";
                               ui->cla->setText(result);
                               }
                               else{
                                    QString result = clas + "\nTú peso máximo ideal es de: " + QString::number(int(pIA)) + "\nTú peso mínimo ideal es de: " + QString::number(int(pIB)) + "\nEstas dentro de tu peso ideal.\nFELICIDADES :)\n";
                                    ui->cla->setText(result);
                               }
                       }

}

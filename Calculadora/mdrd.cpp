#include "mdrd.h"
#include "ui_mdrd.h"
#include <cmath>

mdrd::mdrd(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mdrd)
{
    ui->setupUi(this);
}

mdrd::~mdrd()
{
    delete ui;
}

void mdrd::on_pushButton_clicked()
{
    float ce, ee, mdrd, Edad, Cr, CE, EE, mdrd2;
    QString edad = ui->edad->text();
    Edad = edad.toFloat();
    QString creatinia = ui->cr->text();
    Cr = creatinia.toFloat();
    ce=pow(Cr, -0.999);
    ee=pow(Edad, -0.176);
    mdrd=186*ce*ee;
// Calculo para el MDRD normal
    if(ui->Hombre->isChecked()){
        QString text = "El MDRD Calculado es de: " + QString::number(mdrd) + " ml/min";
        ui->mdr_r->setText(text);
        }
        else{
            mdrd=mdrd*0.742;
            QString text = "El MDRD Calculado es de: " + QString::number(mdrd) + " ml/min";
            ui->mdr_r->setText(text);
            }
//Calculo para el MDRD-IDMS
    CE=pow(Cr, -1.154);
    EE=pow(Edad, -0.203);
    mdrd2=175*CE*EE;
    if(ui->Hombre->isChecked()){
        QString text = "El MDRD-IDMS Calculado es de: " + QString::number(mdrd2) + " ml/min";
        ui->mdr_ids->setText(text);
        }
        else{
            mdrd2=mdrd2*0.742;
            QString text = "El MDRD-IDMS Calculado es de: " + QString::number(mdrd2) + " ml/min";
            ui->mdr_ids->setText(text);
            }
    if (mdrd2 >=90 ){
        ui->res_3->setText("Por MDRD-IDMS se considera estadio 1 de ERC según KDIGO");
        }
        else{
        if((mdrd2 <90)&&(mdrd2 >=60)){
            ui->res_3->setText("Por MDRD-IDMS se considera estadio 2 de ERC según KDIGO");
            }
            else{
            if((mdrd2 <60)&&(mdrd2 >=30)){
                ui->res_3->setText("Por MDRD-IDMS se considera estadio 3 de ERC según KDIGO");
                }
            else{
                if((mdrd2 <30)&&(mdrd2 >=15)){
                    ui->res_3->setText("Por MDRD-IDMS se considera estadio 4 de ERC según KDIGO");
                }
                else {
                    ui->res_3->setText("Por MDRD-IDMS se considera estadio 5 de ERC según KDIGO");
                    }
            }
        }
    }
}

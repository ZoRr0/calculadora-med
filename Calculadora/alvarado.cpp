#include "alvarado.h"
#include "ui_alvarado.h"

alvarado::alvarado(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::alvarado)
{
    ui->setupUi(this);
}

alvarado::~alvarado()
{
    delete ui;
}

void alvarado::on_pushButton_clicked()
{
    //Variables
    int alva,mgd,anore,nauvo,dolor,rebot,temp,leuco,neutro;

    //Asignación de valores según elección

    if (ui->MDS->isChecked()){
        mgd=1;
    }
    else{
        mgd=0;
    }
    if(ui->AS->isChecked()){
        anore=1;
    }
    else{
        anore=0;
    }
    if(ui->NVS->isChecked()){
        nauvo=1;
    }
    else{
        nauvo=0;
    }
    if(ui->DCDS->isChecked()){
        dolor=2;
    }
    else{
        dolor=0;
    }
    if(ui->RS->isChecked()){
        rebot=1;
    }
    else {
        rebot=0;
    }
    if(ui->TS->isChecked()){
        temp=1;
    }
    else{
        temp=0;
    }
    if(ui->LS->isChecked()){
        leuco=2;
    }
    else{
        leuco=0;
    }
    if(ui->NS->isChecked()){
        neutro=1;
    }
    else{
        neutro=0;
    }
    //Formula
    alva=mgd+anore+nauvo+dolor+rebot+temp+leuco+neutro;

    //Resultado y clasificación
    if(alva>=7){
        ui->res->setText("Un total de: " + QString::number(alva) + "\nSe Considera una alta probabilidad\n de Apendicitis ameritando intervención \nquirúrgica.");
    }
    else{
        if(alva>=5 && alva<=6){
            ui->res->setText("Un total de: " + QString::number(alva) + "\nSe Considera un diagnóstico dudoso \nde Apendicitis se sugiere\nexamenes complementarios.");
        }
        else{
            ui->res->setText("Un total de: " + QString::number(alva) + "\nPoca probabilidad de Apendicitis, \nrealizar diferenciales de dolor abdominal.");
        }
    }
}

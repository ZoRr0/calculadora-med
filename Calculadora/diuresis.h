#ifndef PRUEBA_H
#define PRUEBA_H

#include <QWidget>

namespace Ui {
class diuresis;
}

class diuresis : public QWidget
{
    Q_OBJECT

public:
    explicit diuresis(QWidget *parent = 0);
    ~diuresis();

private slots:
    void on_pushButton_clicked();

private:
    Ui::diuresis *ui;
};

#endif // PRUEBA_H

#include "pam.h"
#include "ui_pam.h"

PAM::PAM(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PAM)
{
    ui->setupUi(this);
}

PAM::~PAM()
{
    delete ui;
}

void PAM::on_pushButton_clicked()
{
    //Conversión de Variables
    QString SIS = ui->sisto->text();  // Nombrar con una variable temporal "SIS" y pasar el valor del ui
    QString DIA = ui->diasto->text();
    //Conversión de variables para poder usar
    int Sis = SIS.toInt(); //Nombrar la variable a usar en el código "Sis" y convertir el valor tomado de la variable temporal "SIS" y convertirlo a tipo deseado "int"
    int Dia = DIA.toInt();

    //Formulas
    int pam = ((2*Dia)+Sis)/3;

    //Resultados

    QString text = "La PAM es de: " + QString::number(pam) + " mmHg.";
    ui->res->setText(text);

}

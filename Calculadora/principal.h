#ifndef PRINCIPAL_H
#define PRINCIPAL_H

#include <QMainWindow>
#include <imc.h>
#include "diuresis.h"
#include <mdrd.h>
#include <hco3.h>
#include <pam.h>
#include <hipona.h>
#include <acercad.h>
#include <hiponap.h>
#include <glasgow.h>
#include <alvarado.h>
#include <superficiec.h>
#include <imc-am.h>
#include <calculodosis.h>
#include <velocidadi.h>
#include <solucionpedia.h>

namespace Ui {
class principal;
}

class principal : public QMainWindow
{
    Q_OBJECT

public:
    explicit principal(QWidget *parent = 0);
    ~principal();

private slots:
    void on_imc_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_hco3_clicked();

    void on_pam_clicked();

    void on_hipoNa_clicked();

    void on_acercaD_clicked();

    void on_glasgow_clicked();

    void on_alvarado_clicked();

    void on_SC_clicked();

    void on_imcAM_clicked();

    void on_dosisponderal_clicked();

    void on_pushButton_3_clicked();

    void on_liquidospedia_clicked();

private:
    Ui::principal *ui;
};

#endif // PRINCIPAL_H

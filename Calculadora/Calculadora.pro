#-------------------------------------------------
#
# Project created by QtCreator
#
# CopyrIght 2018 Pedro Pablo Reyes Cameras <pedro.p.rc@riseup.net>
#
# This progrAm is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program.  If not,\nsee <http://www.gnu.org/licenses/> or <https://www.gnu.org/licenses/gpl.txt>.
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Calculadora-MED
TEMPLATE = app


SOURCES += main.cpp\
    calculodosis.cpp \
    imc-am.cpp \
    principal.cpp \
    diuresis.cpp \
    imc.cpp \
    mdrd.cpp \
    hco3.cpp \
    pam.cpp \
    hipona.cpp \
    hiponar.cpp \
    hiponav.cpp \
    acercad.cpp \
    hiponap.cpp \
    glasgow.cpp \
    alvarado.cpp \
    solucionpedia.cpp \
    superficiec.cpp \
    velocidadi.cpp

HEADERS  += principal.h \
    calculodosis.h \
    diuresis.h \
    imc-am.h \
    imc.h \
    mdrd.h \
    hco3.h \
    pam.h \
    hipona.h \
    hiponar.h \
    hiponav.h \
    acercad.h \
    hiponap.h \
    glasgow.h \
    alvarado.h \
    solucionpedia.h \
    superficiec.h \
    velocidadi.h

FORMS    += principal.ui \
    calculodosis.ui \
    diuresis.ui \
    imc-am.ui \
    imc.ui \
    mdrd.ui \
    hco3.ui \
    pam.ui \
    hipona.ui \
    hiponar.ui \
    hiponav.ui \
    acercad.ui \
    hiponap.ui \
    glasgow.ui \
    alvarado.ui \
    solucionpedia.ui \
    superficiec.ui \
    velocidadi.ui

CONFIG += mobility
MOBILITY = 

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

contains(ANDROID_TARGET_ARCH,) {
    ANDROID_ABIS = \
        armeabi-v7a
}


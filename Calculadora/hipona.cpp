#include "hipona.h"
#include "ui_hipona.h"
#include "hiponar.h"
#include "hiponav.h"
#include "hiponap.h"

hipoNa::hipoNa(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::hipoNa)
{
    ui->setupUi(this);
}

hipoNa::~hipoNa()
{
    delete ui;
}

void hipoNa::on_resT_clicked()
{
    hipoNaR *ventana7 = new hipoNaR();

    ventana7->show();

}

void hipoNa::on_velR_clicked()
{
    hipoNaV *ventana8 = new hipoNaV();
    ventana8->show();
}

void hipoNa::on_preS_clicked()
{
    hiponaP *ventana9 = new hiponaP();
    ventana9->show();
}

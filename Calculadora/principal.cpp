#include "principal.h"
#include "ui_principal.h"

principal::principal(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::principal)
{
    ui->setupUi(this);
}

principal::~principal()
{
    delete ui;
}

void principal::on_imc_clicked()
{

    imc *ventana = new imc();
    ventana->show();

}

void principal::on_pushButton_clicked()
{
    diuresis *ventana2 = new diuresis();
    ventana2->show();
}

void principal::on_pushButton_2_clicked()
{
    mdrd *ventana3 = new mdrd();
    ventana3->show();
}

void principal::on_hco3_clicked()
{
    hco3 *ventana4 = new hco3();
    ventana4->show();
}
void principal::on_pam_clicked()
{
    PAM *ventana5 = new PAM();
    ventana5->show();
}

void principal::on_hipoNa_clicked()
{
    hipoNa *ventana6 = new hipoNa();
    ventana6->show();
}
void principal::on_acercaD_clicked()
{
    acercaD *ventana7 = new acercaD();
    ventana7->show();
}

void principal::on_glasgow_clicked()
{
    glasgow *ventana8 = new glasgow();
    ventana8->show();

}

void principal::on_alvarado_clicked()
{
    alvarado *ventana9 = new alvarado();
    ventana9->show();
}

void principal::on_SC_clicked()
{
    superficieC *ventana10 = new superficieC();
    ventana10->show();
}

void principal::on_imcAM_clicked()
{
    imcam *ventana11 = new imcam();
    ventana11->show();
}

void principal::on_dosisponderal_clicked()
{
    calculoDosis *ventana12 = new calculoDosis();
    ventana12->show();
}

void principal::on_pushButton_3_clicked()
{
    velocidadI *ventana13 = new velocidadI();
    ventana13->show();
}

void principal::on_liquidospedia_clicked()
{
    solucionpedia *ventana14 = new solucionpedia();
    ventana14->show();
}

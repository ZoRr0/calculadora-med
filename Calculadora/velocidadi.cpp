#include "velocidadi.h"
#include "ui_velocidadi.h"

velocidadI::velocidadI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::velocidadI)
{
    ui->setupUi(this);
}

velocidadI::~velocidadI()
{
    delete ui;
}

void velocidadI::on_pushButton_clicked()
{
    //Obtener valores de la interfaz y colocarlo en una variable temporal
    QString tcant = ui->cant->text();
    QString tml = ui->ml->text();
    // Convertir las variables temporales a las utilizables en el código
    float tmp = tcant.toInt();
    float Ml = tml.toInt();
    float ml_min;
    float gotas;
    float microG;
    // Formulas
    if(ui->hr->isChecked()){
        tmp= tmp*60;
        ml_min=Ml/tmp;
        gotas = (ml_min*20)/6;
        microG = (ml_min*60)/6;
    }else {
        ml_min=Ml/tmp;
        gotas = (ml_min*20)/6;
        microG = (ml_min*60)/6;
    }
    if(gotas < 1){
        QString resultados = "Tienes menos de 1 gota cada 10 segundos\nrevisa la cantidad o el tiempo especificado, recuerda marcar\nadecuadamente la casilla de hrs o minutos.";
        ui->result->setText(resultados);
    }else {
        QString resultados = "Debes pasar "+QString::number(gotas)+" gotas\nen 10 segundos.\nLo que equivale a "+QString::number(microG)+" micro-gotas.";
        ui->result->setText(resultados);
    }

}

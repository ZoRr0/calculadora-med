#ifndef HIPONAR_H
#define HIPONAR_H

#include <QWidget>

namespace Ui {
class hipoNaR;
}

class hipoNaR : public QWidget
{
    Q_OBJECT

public:
    explicit hipoNaR(QWidget *parent = 0);
    ~hipoNaR();

private slots:
    void on_pushButton_clicked();

private:
    Ui::hipoNaR *ui;
};

#endif // HIPONAR_H

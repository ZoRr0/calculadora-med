#ifndef HCO3_H
#define HCO3_H

#include <QWidget>

namespace Ui {
class hco3;
}

class hco3 : public QWidget
{
    Q_OBJECT

public:
    explicit hco3(QWidget *parent = 0);
    ~hco3();

private slots:
    void on_calcular_clicked();

private:
    Ui::hco3 *ui;
};

#endif // HCO3_H

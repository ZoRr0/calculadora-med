#include "solucionpedia.h"
#include "ui_solucionpedia.h"

solucionpedia::solucionpedia(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::solucionpedia)
{
    ui->setupUi(this);
}

solucionpedia::~solucionpedia()
{
    delete ui;
}

void solucionpedia::on_pushButton_clicked()
{
    //Obtener valores de la interfaz y colocarlo en una variable temporal
    QString peso = ui->peso->text();
    QString rK = ui->rK->text();
    //Convertir las variables temporales a las utilizables en el código
    float Peso = peso.toFloat();
    int RK = rK.toUInt();
    float ST,pesot,STt; //Solución total, peso y solución temporal
    float glu,sf,K,st8; //Soluciones glucosada, salina, potasio y total para 8 hrs
    //Formulas
    if(Peso<=10){
        ST=(Peso*4)*24;
    }else{
        pesot=Peso-10;
        if(pesot<=10){
            ST=((pesot*2)+(10*4))*24;
        }else{
            pesot=Peso-20;
            ST=((pesot*1)+(10*2)+(10*4))*24;
        }
    }
    if(ui->tresuno->isChecked()){
        //Requerimientos para 8 hrs
        STt=ST/4;
        K=((((ST/100)*RK)/3)*10)/20;
        glu=((STt*3)/3)-K;
        sf=STt/3;
        st8=ST/3;
        QString result = "Requerimiento para 8 hrs.\n\nSolución Total..........................."+QString::number(st8)+" ml\nSolución Glucosa al 5%............"+QString::number(glu)+" ml\nSolución Fisiológica al 0.9%...."+QString::number(sf)+" ml\nCloruro de Potasio...................."+QString::number(K)+" ml\n"+"\n"+"* Ampula de KCl 10 ml (20 miliequivalente).";
        ui->resultados->setText(result);
    }else {
        //Requerimientos para 8 hrs
        K=((((ST/100)*RK)/3)*10)/20;
        glu=((ST/2)/3)-K;
        sf=(ST/2)/3;
        st8=ST/3;
        QString result = "Requerimiento para 8 hrs.\n\nSolución Total..........................."+QString::number(st8)+" ml\nSolución Glucosa al 5%............"+QString::number(glu)+" ml\nSolución Fisiológica al 0.9%...."+QString::number(sf)+" ml\nCloruro de Potasio...................."+QString::number(K)+" ml\n"+"\n"+"* Ampula de KCl 10 ml (20 miliequivalente).";
        ui->resultados->setText(result);
        }

}

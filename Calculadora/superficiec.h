#ifndef SUPERFICIEC_H
#define SUPERFICIEC_H

#include <QWidget>

namespace Ui {
class superficieC;
}

class superficieC : public QWidget
{
    Q_OBJECT

public:
    explicit superficieC(QWidget *parent = nullptr);
    ~superficieC();

private slots:

    void on_BotonSC_clicked();

private:
    Ui::superficieC *ui;
};

#endif // SUPERFICIEC_H

#ifndef GLASGOW_H
#define GLASGOW_H

#include <QWidget>

namespace Ui {
class glasgow;
}

class glasgow : public QWidget
{
    Q_OBJECT

public:
    explicit glasgow(QWidget *parent = 0);
    ~glasgow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::glasgow *ui;
};

#endif // GLASGOW_H

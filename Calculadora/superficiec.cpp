#include "superficiec.h"
#include "ui_superficiec.h"

superficieC::superficieC(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::superficieC)
{
    ui->setupUi(this);
}

superficieC::~superficieC()
{
    delete ui;
}
void superficieC::on_BotonSC_clicked()
{
    QString PESO = ui->peso->text();
    double P = PESO.toDouble();
    double SC;
    if (P<10){
        SC=((P*4)+9)/100;
    }
    else{
        SC=((P*4)+7)/(P+90);
    }
    QString text = "La SC es de: " + QString::number(SC) + " m².";
    ui->resp->setText(text);
}

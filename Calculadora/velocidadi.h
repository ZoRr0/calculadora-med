#ifndef VELOCIDADI_H
#define VELOCIDADI_H

#include <QWidget>

namespace Ui {
class velocidadI;
}

class velocidadI : public QWidget
{
    Q_OBJECT

public:
    explicit velocidadI(QWidget *parent = nullptr);
    ~velocidadI();

private slots:
    void on_pushButton_clicked();

private:
    Ui::velocidadI *ui;
};

#endif // VELOCIDADI_H

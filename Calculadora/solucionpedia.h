#ifndef SOLUCIONPEDIA_H
#define SOLUCIONPEDIA_H

#include <QWidget>

namespace Ui {
class solucionpedia;
}

class solucionpedia : public QWidget
{
    Q_OBJECT

public:
    explicit solucionpedia(QWidget *parent = nullptr);
    ~solucionpedia();

private slots:
    void on_pushButton_clicked();

private:
    Ui::solucionpedia *ui;
};

#endif // SOLUCIONPEDIA_H

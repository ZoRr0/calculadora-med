#include "hiponav.h"
#include "ui_hiponav.h"

hipoNaV::hipoNaV(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::hipoNaV)
{
    ui->setupUi(this);
}

hipoNaV::~hipoNaV()
{
    delete ui;
}

void hipoNaV::on_pushButton_clicked()
{
    //Conversión de Variables
    QString MEQ = ui->meq->text();
    QString ML = ui->ml->text();
    QString MIN = ui->min->text();
    QString MEQT= ui->meqT->text();

    //Conversión y declaración de variables
    float meq = MEQ.toFloat();
    float ml = ML.toFloat();
    float min = MIN.toFloat();
    float meqt = MEQT.toFloat();
    float R;

    //Formulas
    R=(meq*ml)/meqt;

    //Presentación de Resultados
    QString text = "Si quieres restituir " + QString::number(meq) +" mEq en "+ QString::number(min) +"\nminutos, de vez pasar " + QString::number(R) + " ml \nde tus " + QString::number(ml) + " ml de solución preparada";
    ui->res->setText(text);
}

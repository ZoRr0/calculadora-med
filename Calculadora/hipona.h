#ifndef HIPONA_H
#define HIPONA_H

#include <QWidget>

namespace Ui {
class hipoNa;
}

class hipoNa : public QWidget
{
    Q_OBJECT

public:
    explicit hipoNa(QWidget *parent = 0);
    ~hipoNa();

private slots:
    void on_resT_clicked();

    void on_velR_clicked();

    void on_preS_clicked();

private:
    Ui::hipoNa *ui;
};

#endif // HIPONA_H

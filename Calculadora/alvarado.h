#ifndef ALVARADO_H
#define ALVARADO_H

#include <QWidget>

namespace Ui {
class alvarado;
}

class alvarado : public QWidget
{
    Q_OBJECT

public:
    explicit alvarado(QWidget *parent = 0);
    ~alvarado();

private slots:
    void on_pushButton_clicked();

private:
    Ui::alvarado *ui;
};

#endif // ALVARADO_H

#ifndef HIPONAV_H
#define HIPONAV_H

#include <QWidget>

namespace Ui {
class hipoNaV;
}

class hipoNaV : public QWidget
{
    Q_OBJECT

public:
    explicit hipoNaV(QWidget *parent = 0);
    ~hipoNaV();

private slots:
    void on_pushButton_clicked();

private:
    Ui::hipoNaV *ui;
};

#endif // HIPONAV_H

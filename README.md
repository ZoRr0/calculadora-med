# Calculadora-MED
## Introducción.

**Calculadora-MED** es un programa diseñado para el uso del día a día en la clínica médica, orientado principalmente para el personal de salud aunque puede ser usado por cualquier persona; sin embargo las decisiones médicas u otras no deben basarse en los resultados de esta aplicación. Aunque ha sido probada, no podemos garantizar la exactitud de sus cálculos o resultados.

**Calculadora-MED** cuenta con varias formulas estandarizadas en el uso de la salud, existen miles de aplicaciones parecidas ya en el mercado, sin embargo **Calculadora-MED** es diferente al ser 100% [**Software Libre**](https://es.wikipedia.org/wiki/Software_libre) y estar en español de manera nativa (sin embargo se espera poder realizar versiones en otros idiomas).

Escrita usando el programa [**Qt Creator**](https://es.wikipedia.org/wiki/Qt_Creator) en [c++](https://es.wikipedia.org/wiki/C%2B%2B) es posible realizar su compilación en los diferentes [**SO**](https://es.wikipedia.org/wiki/Sistema_operativo) como [**GNU/Linux**](https://es.wikipedia.org/wiki/GNU/Linux), [Windows](https://es.wikipedia.org/wiki/Microsoft_Windows), [Mac_OS-X](https://es.wikipedia.org/wiki/MacOS),  [Android](https://es.wikipedia.org/wiki/Android) e [iOS](https://es.wikipedia.org/wiki/IOS) usando el mismo código fuente para cada uno de ellos con la intención de que cualquiera pueda modificar, compilar y ejecutar **Calculadora-MED** independientemente de su SO preferido.

## Formulas en **Calculadora-MED** .

 - [IMC](https://es.wikipedia.org/wiki/%C3%8Dndice_de_masa_corporal)
 - [Escala de Glasgow](https://es.wikipedia.org/wiki/Escala_de_coma_de_Glasgow)
 - [Escala de Alvarado](https://es.wikipedia.org/wiki/Escala_de_Alvarado)
 - Dosis ponderal (Calculo de medicación a partir de la dosis ponderal y presentación del medicamento)
 - [Velocidad de Infusión](https://enfermerix.blogspot.com/2011/03/formula-para-infusiones-endovenosas-con.html)
 - [Calculo de solución pediátrica, por formula de Holliday-Segar](https://medicinacriticapediatrica.files.wordpress.com/2013/06/cc3a1lculo-de-lc3adquidos-en-pediatrc3ada.pdf) 
 - [IMC-Adulto Mayor](https://www.imbiomed.com.mx/1/1/articulos.php?method=showDetail&id_articulo=108397&id_seccion=5800&id_ejemplar=10560&id_revista=378)
 - Superficie Corporal
 - Diuresis
 - [MDRD](https://es.wikipedia.org/wiki/Tasa_de_filtraci%C3%B3n_glomerular#F%C3%B3rmula_MDRD)
 - [PAM](https://es.wikipedia.org/wiki/Presi%C3%B3n_arterial_media)
 - Hiponatremia (*se añadirá la documentación apropiada para el uso de este apartado, formulas basadas en la bibliografía de **Manual Washington de terapéutica Médica, 34° edición, 2014, pag: 405 - 411, Tema: Hiponatremia**.*)
 - Déficit de Bicarbonato(**Manual Washington de terapéutica Médica, 34° edición, 2014**)

## Compilación.

Para poder ejecutar el código de **Calculadora-MED** es necesario instalar el IDE de Qt Creator ([descarga](https://www.qt.io/download)), una vez instalado se clona el repositorio y al ejecutar el IDE se debe seleccionar la opción de abrir proyecto y elegir el archivo **Calculadora.pro** que se encuentra dentro de la carpeta del código fuente, una vez hecho esto el IDE le pedirá configurar el proyecto según los objetivos deseados (móvil o PC), pasando esta simple configuración usted podrá ejecutar el código directamente desde el IDE o compilarlo para crear los binarios y ejecutarlos posteriormente, crear nuevos paquetes con ellos o compartirlos.

## Binarios.

En la carpeta binarios podrás encontrar los compilados para Android (versión 7 o superior) y Linux de 64 bits(necesario Qt_5.12.5), únicamente para estas dos plataformas por el momento.

## Acerca de.

Dentro del mismo programa se puede encontrar esta sección la cual muestra la licencia, y resúmenes de los cambios realizados entre versiones.

## Gracias y contacto.

Esperamos que este programa se de su utilidad y junto con la comunidad pueda ir creciendo, si se tiene comentarios o sugerencias pueden contactar por correo (pedro.p.rc@riseup.net) o por [telegram](https://telegram.org) desde este [link](https://t.me/ZoRrO08)

## Donación.

Si deseas y puedes apoyar económicamente con cualquier cantidad, sera mas que bien recibida, gracias.

Dirección de **BTC**: 3B6nZXEyT63raDvz3q9X8qDtUwLGEGa46K

### Quis custodiet ipsos custodes? :)
